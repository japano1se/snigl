#ifndef SNIGL_VAL_H
#define SNIGL_VAL_H

#include "snigl/fix.h"
#include "snigl/int.h"
#include "snigl/ls.h"
#include "snigl/pair.h"

struct sgl;
struct sgl_buf;
struct sgl_cemit;
struct sgl_error;
struct sgl_file;
struct sgl_func;
struct sgl_iter;
struct sgl_list;
struct sgl_op_lambda;
struct sgl_pos;
struct sgl_str;
struct sg_struct;

struct sgl_val {
  struct sgl_ls ls;
  struct sgl_type *type;
  struct sgl_op *op;
  
  union {
    bool as_bool;
    struct sgl_buf *as_buf;
    char as_char;
    sgl_int_t as_int;
    void *as_ptr;

    struct sgl_error *as_error;
    struct sgl_file *as_file;
    struct sgl_fix as_fix;
    struct sgl_func *as_func;
    struct sgl_iter *as_iter;
    struct sgl_op_lambda *as_lambda;
    struct sgl_list *as_list;
    struct sgl_pair as_pair;
    struct sgl_str *as_str;
    struct sgl_struct *as_struct;
    struct sgl_sym *as_sym;
    struct sgl_type *as_type;
  };
};

struct sgl_val *sgl_val_init(struct sgl_val *val,
                             struct sgl *sgl,
                             struct sgl_type *type,
                             struct sgl_ls *root);

struct sgl_val *sgl_val_reuse(struct sgl_val *val,
                              struct sgl *sgl,
                              struct sgl_type *type,
                              bool deinit);

struct sgl_val *sgl_val_deinit(struct sgl_val *val, struct sgl *sgl);

struct sgl_val *sgl_val_new(struct sgl *sgl,
                            struct sgl_type *type,  
                            struct sgl_ls *root);

void sgl_val_free(struct sgl_val *val, struct sgl *sgl);

bool sgl_val_bool(struct sgl_val *val);

struct sgl_op *sgl_val_call(struct sgl_val *val,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_op *return_pc,
                            bool now);

struct sgl_val *sgl_val_clone(struct sgl_val *val,
                              struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_ls *root);

enum sgl_cmp sgl_val_cmp(struct sgl_val *val, struct sgl_val *rhs);
void sgl_val_dump(struct sgl_val *val, struct sgl_buf *out);

struct sgl_val *sgl_val_dup(struct sgl_val *val,
                            struct sgl *sgl,
                            struct sgl_ls *root);

bool sgl_val_eq(struct sgl_val *val, struct sgl *sgl, struct sgl_val *rhs);
bool sgl_val_is(struct sgl_val *val, struct sgl_val *rhs);
struct sgl_iter *sgl_val_iter(struct sgl_val *val,
                              struct sgl *sgl,
                              struct sgl_type **type);

void sgl_val_print(struct sgl_val *val,
                   struct sgl *sgl,
                   struct sgl_pos *pos,
                   struct sgl_buf *out);

bool sgl_val_cemit(struct sgl_val *val,
                   struct sgl *sgl,
                   struct sgl_pos *pos,
                   const char *id,
                   const char *root,
                   struct sgl_cemit *out);
                   
#endif
