#ifndef SNIGL_FIX_H
#define SNIGL_FIX_H

#include <stdbool.h>

#include "snigl/int.h"

struct sgl_buf;

struct sgl_fix {
  sgl_int_t val, scale;
};

struct sgl_fix *sgl_fix_init(struct sgl_fix *f,
                             sgl_int_t trunc, sgl_int_t frac,
                             sgl_int_t scale,
                             bool neg);

bool sgl_fix_is(struct sgl_fix *f, struct sgl_fix *rhs);
bool sgl_fix_eq(struct sgl_fix *f, struct sgl_fix *rhs);
enum sgl_cmp sgl_fix_cmp(struct sgl_fix *f, struct sgl_fix *rhs);
void sgl_fix_add(struct sgl_fix *f, struct sgl_fix *rhs);
void sgl_fix_sub(struct sgl_fix *f, struct sgl_fix *rhs);
void sgl_fix_mul(struct sgl_fix *f, struct sgl_fix *rhs);
void sgl_fix_int_mul(struct sgl_fix *f, sgl_int_t rhs);
void sgl_fix_div(struct sgl_fix *f, struct sgl_fix *rhs);
sgl_int_t sgl_fix_int(struct sgl_fix *f);
void sgl_fix_dump(struct sgl_fix *f, struct sgl_buf *out);
void sgl_fix_print(struct sgl_fix *f, struct sgl_buf *out);

#endif
