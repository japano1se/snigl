#ifndef SNIGL_FUNC_H
#define SNIGL_FUNC_H

#include "snigl/arg.h"
#include "snigl/def.h"
#include "snigl/lset.h"

#define sgl_func_call(f, sgl, now, ...)                                \
  _sgl_func_call(f, sgl, now, (struct sgl_val *[]){__VA_ARGS__, NULL}) \

struct sgl;
struct sgl_cemit;
struct sgl_fimp;
struct sgl_type;

struct sgl_func {
  struct sgl_def def;
  struct sgl_ls ls;
  struct sgl_lset fimps, fimp_queue;
  sgl_int_t cemit_id, nargs;
};

extern struct sgl_def_type SGL_DEF_FUNC;

struct sgl_func *sgl_func_init(struct sgl_func *func,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_lib *lib,
                               struct sgl_sym *id,
                               sgl_int_t nargs);

struct sgl_func *sgl_func_deinit(struct sgl_func *func, struct sgl *sgl);

struct sgl_func *sgl_func_new(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_lib *lib,
                              struct sgl_sym *id,
                              sgl_int_t nargs);

sgl_int_t sgl_func_cemit_id(struct sgl_func *f,
                            struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_cemit *out);

struct sgl_fimp *sgl_func_add_fimp(struct sgl_func *func,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_arg args[],
                                   struct sgl_type *rets[]);

struct sgl_fimp *sgl_func_get_fimp(struct sgl_func *func,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_sym *id);

struct sgl_fimp *sgl_func_fimp(struct sgl_func *func);

struct sgl_fimp *sgl_func_dispatch(struct sgl_func *func,
                                   struct sgl *sgl,
                                   struct sgl_ls *stack);

struct sgl_fimp *sgl_func_dispatch_types(struct sgl_func *func,
                                         struct sgl *sgl,
                                         struct sgl_ls *stack);

struct sgl_op *_sgl_func_call(struct sgl_func *f,
                              struct sgl *sgl,
                              bool now,
                              struct sgl_val *args[]);

void sgl_setup_funcs();

#endif
