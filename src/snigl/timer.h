#ifndef SNIGL_TIMER_H
#define SNIGL_TIMER_H

#include <time.h>

#include "snigl/int.h"

struct sgl_timer {
  clock_t start;
};

struct sgl_timer *sgl_timer_init(struct sgl_timer *timer);
sgl_int_t sgl_timer_ms(struct sgl_timer *timer);

#endif
