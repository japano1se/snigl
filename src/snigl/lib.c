#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include "snigl/def.h"
#include "snigl/form.h"
#include "snigl/func.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/vec.h"

static enum sgl_cmp defs_cmp(const void *lhs, const void *rhs, void *_) {
  return sgl_sym_cmp(*(struct sgl_sym **)lhs, *(struct sgl_sym **)rhs);
}

static const void *defs_key(void *val) {
  struct sgl_def **d = val;
  return &(*d)->id;
}

struct sgl_lib *sgl_lib_init(struct sgl_lib *lib,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id) {
  lib->id = id;
  lib->init = NULL;
  lib->deinit = NULL;
  sgl_vset_init(&lib->defs, sizeof(struct sgl_def *), defs_cmp, 0);
  lib->defs.key_fn = defs_key;
  lib->init_ok = false;
  return lib;
}

struct sgl_lib *sgl_lib_deinit1(struct sgl_lib *lib, struct sgl *sgl) {
  sgl_vset_do(&lib->defs, struct sgl_def *, d) {
    if ((*d)->type != &SGL_DEF_TYPE) {
      sgl_def_deref(*d, sgl);
      *d = NULL;
    }
  }
  
  return lib;
}

struct sgl_lib *sgl_lib_deinit2(struct sgl_lib *lib, struct sgl *sgl) {
  sgl_vset_do(&lib->defs, struct sgl_def *, d) {
    if (*d) { sgl_def_deref(*d, sgl); }
  }
  
  sgl_vset_deinit(&lib->defs);
  if (lib->deinit) { lib->deinit(lib, sgl); }    
  return lib;
}

struct sgl_lib *sgl_lib_new(struct sgl *sgl,
                            struct sgl_pos *pos,
                            struct sgl_sym *id) {
  return sgl_lib_init(sgl_malloc(&sgl->lib_pool), sgl, pos, id);
}

bool sgl_lib_add(struct sgl_lib *lib,
                 struct sgl *sgl,
                 struct sgl_pos *pos,
                 struct sgl_def *def) {
  bool ok = false;
  sgl_int_t i = sgl_vset_find(&lib->defs, &def->id, NULL, &ok);

  if (ok) {
    sgl_error(sgl, pos, sgl_sprintf("Dup def: %s", def->id->id));
    return false;
  }
  
  *(struct sgl_def **)sgl_vec_insert(&lib->defs.vals, i) = def;
  return true;
}

static struct sgl_ls *const_imp(struct sgl_macro *macro,
                                struct sgl_form *form,
                                struct sgl_ls *root,
                                struct sgl *sgl) {
  struct sgl_val *v = sgl_baseof(struct sgl_val, ls, macro->data.next);
  sgl_op_push_new(sgl, form, sgl_val_dup(v, sgl, NULL));
  return form->ls.next;
}

static void const_deinit(struct sgl_macro *m, struct sgl *sgl) {
  sgl_ls_do(&m->data, struct sgl_val, ls, v) { sgl_val_deinit(v, sgl); }
}

bool sgl_lib_add_const(struct sgl_lib *l,
                       struct sgl *sgl,
                       struct sgl_pos *pos,
                       struct sgl_sym *id,
                       struct sgl_val *val) {
  struct sgl_macro *m = sgl_lib_add_macro(l, sgl, pos, id, 0, const_imp);
  m->deinit = const_deinit;
  sgl_ls_push(&m->data, &val->ls);
  return true;
}

struct sgl_macro *sgl_lib_add_macro(struct sgl_lib *l,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_sym *id,
                                    sgl_int_t nargs,
                                    sgl_macro_imp_t imp) {
  struct sgl_macro *m = sgl_macro_new(sgl, pos, l, id, nargs, imp);
  sgl_lib_add(l, sgl, pos, &m->def);
  return m;
}

struct sgl_fimp *_sgl_lib_add_func(struct sgl_lib *lib,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_sym *id,
                                   sgl_int_t nargs,
                                   struct sgl_arg args[],
                                   struct sgl_type *rets[]) {
  struct sgl_def *d = sgl_lib_find(lib, sgl, pos, id);

  if (d && d->type != &SGL_DEF_FUNC) {
    sgl_error(sgl, pos,
              sgl_sprintf("Func def mismatch (%s): %s", d->type->id, id->id));
    
    return NULL;
  }
  
  struct sgl_func *func = d ? sgl_baseof(struct sgl_func, def, d) : NULL;
  
  
  if (func) {
    if (func->nargs != nargs) {
      sgl_error(sgl, pos,
                sgl_sprintf("Func args mismatch (%" SGL_INT "/%" SGL_INT "): %s",
                            nargs, func->nargs, id->id));
      
      return NULL;
    }
  } else {
    func = sgl_func_new(sgl, pos, lib, id, nargs);
    sgl_lib_add(lib, sgl, pos, &func->def);
  }
  
  return sgl_func_add_fimp(func, sgl, pos, args, rets);
}

struct sgl_fimp *_sgl_lib_add_cfunc(struct sgl_lib *lib,
                                    struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_sym *id,
                                    sgl_int_t nargs,
                                    struct sgl_arg args[],
                                    struct sgl_type *rets[],
                                    sgl_cimp_t imp) {
  struct sgl_fimp *f = _sgl_lib_add_func(lib, sgl, pos, id, nargs, args, rets);
  if (!f) { return NULL; }
  f->cimp = imp;
  return f;
}

static struct sgl_def *new_type_union(struct sgl_lib *lib,
                                      struct sgl *sgl,
                                      struct sgl_pos *pos,
                                      struct sgl_sym *id) {
  char *start = id->id;
  struct sgl_vec pts;
  sgl_vec_init(&pts, sizeof(struct sgl_type *));

  for (;;) {
    char *end = strchr(start, '|');
    struct sgl_sym *pid = end ? sgl_nsym(sgl, start, end-start) : sgl_sym(sgl, start);
    struct sgl_type *pt = sgl_lib_find_type(lib, sgl, pos, pid);

    if (!pt) {
      sgl_error(sgl, pos, sgl_sprintf("Missing type: %s", pid->id));
      sgl_vec_deinit(&pts);
      return NULL;
    }
    
    *(struct sgl_type **)sgl_vec_push(&pts) = pt;
    if (!end) { break; }
    start = end+1;
  }

  
  *(struct sgl_type **)sgl_vec_push(&pts) = NULL;
  struct sgl_type *u = sgl_type_union_new(sgl, pos, lib, id, sgl_vec_beg(&pts));
  sgl_vec_deinit(&pts);
  return &u->def;
}

struct sgl_def *sgl_lib_find(struct sgl_lib *lib,
                             struct sgl *sgl,
                             struct sgl_pos *pos,
                             struct sgl_sym *id) {
  bool ok = false;
  sgl_int_t i = sgl_vset_find(&lib->defs, &id, NULL, &ok);

  if (!ok && isupper(id->id[0]) && strchr(id->id, '|')) {
    return new_type_union(lib, sgl, pos, id);
  }

  return ok ? *(struct sgl_def **)sgl_vec_get(&lib->defs.vals, i) : NULL;
}

struct sgl_type *sgl_lib_find_type(struct sgl_lib *lib,
                                   struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_sym *id) {     
  struct sgl_def *d = sgl_lib_find(sgl->task->lib, sgl, pos, id);
  if (!d) { return NULL; }
  assert(d->type == &SGL_DEF_TYPE);
  return sgl_baseof(struct sgl_type, def, d);
}

bool sgl_lib_use(struct sgl_lib *lib,
                 struct sgl *sgl,
                 struct sgl_pos *pos,
                 struct sgl_def *def) {
  bool exists = false;
  sgl_int_t i = sgl_vset_find(&lib->defs, &def->id, NULL, &exists);

  if (exists) {
    sgl_error(sgl, pos, sgl_sprintf("Dup def: %s", def->type->id));
    return false;
  }

  *(struct sgl_def **)sgl_vec_insert(&lib->defs.vals, i) = def;
  def->nrefs++;
  return true;
}
