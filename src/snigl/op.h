#ifndef SNIGL_OP_H
#define SNIGL_OP_H

#include "snigl/ls.h"

struct sgl;
struct sgl_buf;
struct sgl_cemit;
struct sgl_form;
struct sgl_field;
struct sgl_trace;
struct sgl_val;

struct sgl_op_bench { struct sgl_op *end_pc; };
struct sgl_op_call { struct sgl_val *val; };
struct sgl_op_fimp_call { struct sgl_fimp *fimp; };
struct sgl_op_count { struct sgl_op *pc; };

struct sgl_op_dispatch {
  struct sgl_func *func;
  struct sgl_fimp *fimp;
};

struct sgl_op_drop { sgl_int_t nvals; };
struct sgl_op_eval_beg { struct sgl_op *end_pc; };

struct sgl_op_field {
  struct sgl_type *struct_type;
  struct sgl_field *field;
  struct sgl_sym *id;
  struct sgl_val *val;
};

struct sgl_op_fimp { struct sgl_fimp *fimp; };
struct sgl_op_for { struct sgl_op *end_pc; };

struct sgl_op_get_var {
  struct sgl_sym *id;
  struct sgl_val *val;
};

struct sgl_op_idle { struct sgl_op *end_pc; };

struct sgl_op_if {
  bool neg;
  struct sgl_op *pc;
};

struct sgl_op_iter { struct sgl_op *pc; };
struct sgl_op_jump { struct sgl_op *pc; };

struct sgl_op_let_var {
  struct sgl_sym *id;
  struct sgl_val *val;
};

struct sgl_op_lambda {
  struct sgl_op *start_pc, *end_pc;
  struct sgl_type *type;
  bool push_val;
};

struct sgl_op_push { struct sgl_val *val; };
struct sgl_op_recall { struct sgl_func *func; };
struct sgl_op_str_put { struct sgl_val *val; };

struct sgl_op_scope_beg {
  struct sgl_scope *parent;
};

struct sgl_op_stack_switch {
  sgl_int_t n;
  bool pop;
};

struct sgl_op_task_beg {
  struct sgl_op *end_pc;
  bool is_scope;
};

struct sgl_op_throw { struct sgl_val *val; };
struct sgl_op_times { struct sgl_val *reps; };
struct sgl_op_try_beg { struct sgl_op *end_pc; };

struct sgl_op;

struct sgl_op_type {
  const char *id;
  struct sgl_op *(*cinit_op)(struct sgl_op *, struct sgl *, struct sgl_cemit *);
  struct sgl_op *(*cemit_op)(struct sgl_op *, struct sgl *, struct sgl_cemit *);
  bool (*const_op)(struct sgl_op *);
  void (*deinit_op)(struct sgl_op *, struct sgl *);
  void (*deval_op)(struct sgl_op *, struct sgl *);
  void (*dump_op)(struct sgl_op *, struct sgl_buf *);
  bool (*trace_op)(struct sgl_op *, struct sgl *, struct sgl_trace *);
};

struct sgl_op_type *sgl_op_type_init(struct sgl_op_type *type, const char *id);

extern struct sgl_op_type SGL_NOP,
  SGL_OP_BENCH,
  SGL_OP_CALL, SGL_OP_COUNT,
  SGL_OP_DISPATCH, SGL_OP_DROP, SGL_OP_DUP,
  SGL_OP_EVAL_BEG, SGL_OP_EVAL_END, SGL_OP_FIMP, SGL_OP_FIMP_CALL, SGL_OP_FOR,
  SGL_OP_GET_FIELD, SGL_OP_GET_VAR,
  SGL_OP_IDLE, SGL_OP_IF, SGL_OP_ITER,
  SGL_OP_JUMP,
  SGL_OP_LAMBDA, SGL_OP_LET_VAR, SGL_OP_LIST_BEG, SGL_OP_LIST_END,
  SGL_OP_PAIR, SGL_OP_PUSH, SGL_OP_PUT_FIELD,
  SGL_OP_RECALL, SGL_OP_RETURN, SGL_OP_ROTL, SGL_OP_ROTR,
  SGL_OP_SCOPE_BEG, SGL_OP_SCOPE_END, SGL_OP_STACK_SWITCH, SGL_OP_STR_BEG,
  SGL_OP_STR_END, SGL_OP_STR_PUT, SGL_OP_SWAP,
  SGL_OP_TASK_BEG, SGL_OP_TASK_END,
  SGL_OP_THROW, SGL_OP_TIMES,
  SGL_OP_TRY_BEG, SGL_OP_TRY_END,
  SGL_OP_YIELD;

struct sgl_op {
  struct sgl_form *form;
  struct sgl_ls ls;
  struct sgl_op_type *type;
  struct sgl_op *prev, *next;
  sgl_int_t pc, cemit_pc;
  
  struct sgl_op *(*run)(struct sgl_op *, struct sgl *);

  union {
    struct sgl_op_bench as_bench;
    struct sgl_op_call as_call;
    struct sgl_op_count as_count;
    struct sgl_op_dispatch as_dispatch;
    struct sgl_op_drop as_drop;
    struct sgl_op_eval_beg as_eval_beg;
    struct sgl_op_fimp as_fimp;
    struct sgl_op_fimp_call as_fimp_call;
    struct sgl_op_for as_for;
    struct sgl_op_field as_field;
    struct sgl_op_get_var as_get_var;
    struct sgl_op_idle as_idle;
    struct sgl_op_if as_if;
    struct sgl_op_iter as_iter;
    struct sgl_op_jump as_jump;
    struct sgl_op_lambda as_lambda;
    struct sgl_op_let_var as_let_var;
    struct sgl_op_push as_push;
    struct sgl_op_recall as_recall;
    struct sgl_op_scope_beg as_scope_beg;
    struct sgl_op_stack_switch as_stack_switch;
    struct sgl_op_str_put as_str_put;
    struct sgl_op_task_beg as_task_beg;
    struct sgl_op_throw as_throw;
    struct sgl_op_times as_times;
    struct sgl_op_try_beg as_try_beg;
  };
};

struct sgl_op *sgl_op_init(struct sgl_op *op,
                           struct sgl *sgl,
                           struct sgl_form *form,
                           struct sgl_op_type *type);

struct sgl_op *sgl_op_deinit(struct sgl_op *op, struct sgl *sgl);

struct sgl_op *sgl_op_new(struct sgl *sgl,
                          struct sgl_form *form,
                          struct sgl_op_type *type);

bool sgl_op_is_const(struct sgl_op *op);
void sgl_op_free(struct sgl_op *op, struct sgl *sgl);
bool sgl_op_is_val(struct sgl_op *op);

struct sgl_op *sgl_op_cinit(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_cemit *out);

struct sgl_op *sgl_op_cemit(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_cemit *cemit);

void sgl_op_dump(struct sgl_op *op, struct sgl_buf *out);
bool sgl_op_trace(struct sgl_op *op, struct sgl *sgl, struct sgl_trace *t);

bool sgl_ops_recalls(struct sgl_op *start);

struct sgl_op *sgl_op_bench_new(struct sgl *sgl, struct sgl_form *form);

void sgl_op_call_reuse(struct sgl_op *op, struct sgl *sgl);

struct sgl_op *sgl_op_count_new(struct sgl *sgl,
                                struct sgl_form *form,
                                struct sgl_op *pc);

struct sgl_op *sgl_op_dispatch_new(struct sgl *sgl,
                                   struct sgl_form *form,
                                   struct sgl_func *func,
                                   struct sgl_fimp *fimp);

struct sgl_op *sgl_op_drop_new(struct sgl *sgl,
                               struct sgl_form *form,
                               sgl_int_t nvals);

struct sgl_op *sgl_op_dup_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_eval_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_eval_end_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_fimp_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_fimp *fimp);

struct sgl_op *sgl_op_fimp_call_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_fimp *fimp);

struct sgl_op *sgl_op_get_field_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_sym *id);

void sgl_op_fimp_call_reuse(struct sgl_op *op,
                            struct sgl *sgl,
                            struct sgl_fimp *fimp);

struct sgl_op *sgl_op_for_new(struct sgl *sgl,
                              struct sgl_form *form,
                              struct sgl_op *end_pc);

struct sgl_op *sgl_op_get_var_new(struct sgl *sgl,
                                  struct sgl_form *form,
                                  struct sgl_sym *id);

struct sgl_op *sgl_op_idle_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_if_new(struct sgl *sgl,
                             struct sgl_form *form,
                             struct sgl_op *pc);

struct sgl_op *sgl_op_iter_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_op *pc);

struct sgl_op *sgl_op_jump_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_op *pc);

struct sgl_op *sgl_op_lambda_new(struct sgl *sgl,
                                 struct sgl_form *form,
                                 bool is_nil);

struct sgl_op *sgl_op_let_var_new(struct sgl *sgl,
                                  struct sgl_form *form,
                                  struct sgl_sym *id);

struct sgl_op *sgl_op_list_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_list_end_new(struct sgl *sgl, struct sgl_form *form);

void sgl_op_deval(struct sgl_op *op, struct sgl *sgl);
void sgl_nop(struct sgl_op *op, struct sgl *sgl);
struct sgl_op *sgl_nop_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_pair_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_push_new(struct sgl *sgl,
                               struct sgl_form *form,
                               struct sgl_val *val);

void sgl_op_push_reuse(struct sgl_op *op, struct sgl *sgl, struct sgl_val *val);

struct sgl_op *sgl_op_put_field_new(struct sgl *sgl,
                                    struct sgl_form *form,
                                    struct sgl_sym *id,
                                    struct sgl_val *val);

struct sgl_op *sgl_op_recall_new(struct sgl *sgl,
                                 struct sgl_form *form,
                                 struct sgl_func *func);

struct sgl_op *sgl_op_return_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_rotl_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_rotr_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_scope_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_scope_end_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_stack_switch_new(struct sgl *sgl,
                                       struct sgl_form *form,
                                       sgl_int_t n,
                                       bool pop);

struct sgl_op *sgl_op_str_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_str_put_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_str_end_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_swap_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_task_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_task_end_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_throw_new(struct sgl *sgl, struct sgl_form *form);

struct sgl_op *sgl_op_times_new(struct sgl *sgl,
                                struct sgl_form *form,
                                struct sgl_val *reps);

struct sgl_op *sgl_op_try_beg_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_try_end_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_type_new(struct sgl *sgl, struct sgl_form *form);
struct sgl_op *sgl_op_yield_new(struct sgl *sgl, struct sgl_form *form);

void sgl_setup_ops();

#endif
