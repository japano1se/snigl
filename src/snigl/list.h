#ifndef SNIGL_LIST_H
#define SNIGL_LIST_H

#include "snigl/iter.h"
#include "snigl/ls.h"

struct sgl;

struct sgl_list {
  struct sgl_ls ls, root;
  sgl_int_t nrefs;
};

struct sgl_list *sgl_list_new(struct sgl *sgl);
void sgl_list_deref(struct sgl_list *l, struct sgl *sgl);
struct sgl_list *sgl_list_init(struct sgl_list *l);
struct sgl_list *sgl_list_deinit(struct sgl_list *l, struct sgl *sgl);

sgl_int_t sgl_list_len(struct sgl_list *l);

struct sgl_list_iter {
  struct sgl_iter iter;
  struct sgl_list *list;
  struct sgl_ls *pos;
};

struct sgl_list_iter *sgl_list_iter_new(struct sgl *sgl, struct sgl_list *l);

#endif
