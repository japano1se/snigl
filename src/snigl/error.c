#include <stdlib.h>

#include "snigl/buf.h"
#include "snigl/error.h"
#include "snigl/pool.h"
#include "snigl/sgl.h"
#include "snigl/util.h"
#include "snigl/val.h"

struct sgl_error *sgl_error_new(struct sgl *sgl,
                                struct sgl_pos *pos,
                                char *msg,
                                struct sgl_val *val,
                                struct sgl_ls *root) {
  return sgl_error_init(sgl_malloc(&sgl->error_pool), pos, msg, val, root);
}

struct sgl_error *sgl_error_init(struct sgl_error *e,
                                 struct sgl_pos *pos,
                                 char *msg,
                                 struct sgl_val *val,
                                 struct sgl_ls *root) {  
  if (root) {
    sgl_ls_push(root, &e->ls);
  } else {
    sgl_ls_init(&e->ls);
  }

  e->pos = pos ? *pos : SGL_NULL_POS;
  e->msg = msg;
  e->val = val;
  e->nrefs = 1;
  return e;
}

struct sgl_error *sgl_error_deinit(struct sgl_error *e, struct sgl *sgl) {
  if (e->msg) { free(e->msg); }
  if (e->val) { sgl_val_free(e->val, sgl); }
  sgl_ls_delete(&e->ls);
  return e;
}

void sgl_error_free(struct sgl_error *e, struct sgl *sgl) {
  sgl_free(&sgl->error_pool, sgl_error_deinit(e, sgl));
}

void sgl_error_deref(struct sgl_error *e, struct sgl *sgl) {
  if (!--e->nrefs) { sgl_error_free(e, sgl); }
}

void sgl_error_dump(struct sgl_error *e, struct sgl_buf *out) {
  sgl_buf_printf(out, "Error in row %" SGL_INT ", col %" SGL_INT,
                 e->pos.row, e->pos.col);
  
  if (e->msg) { sgl_buf_printf(out, ": %s", e->msg); }

  if (e->val) {
    sgl_buf_putcs(out, ": \n");
    sgl_val_dump(e->val, out);
  } 
}
