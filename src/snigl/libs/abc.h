#ifndef SNIGL_LIBS_ABC_H
#define SNIGL_LIBS_ABC_H

struct sgl_lib;

struct sgl_lib *sgl_abc_lib_new(struct sgl *sgl, struct sgl_pos *pos);

#endif
