#ifndef SNIGL_VAR_H
#define SNIGL_VAR_H

#include "snigl/ls.h"

struct sgl_var {
  struct sgl_ls ls;
  struct sgl_sym *id;
  struct sgl_val *val;
};

enum sgl_cmp sgl_var_cmp(struct sgl_ls *lhs, const void *rhs, void *_);

struct sgl_var *sgl_var_init(struct sgl_var *v,
                             struct sgl_sym *id,
                             struct sgl_val *val);

struct sgl_var *sgl_var_deinit(struct sgl_var *v, struct sgl *sgl);

struct sgl_var *sgl_var_new(struct sgl *sgl, struct sgl_sym *id, struct sgl_val *val);
void sgl_var_free(struct sgl_var *v, struct sgl *sgl);

#endif
