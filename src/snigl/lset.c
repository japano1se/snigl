#include <stddef.h>

#include "snigl/lset.h"

struct sgl_lset *sgl_lset_init(struct sgl_lset *s, sgl_ls_cmp_t cmp) {
  s->cmp = cmp;
  sgl_ls_init(&s->root);
  return s;
}

struct sgl_ls *sgl_lset_find(struct sgl_lset *s,
                             const void *key,
                             void *data,
                             bool *ok) {
  struct sgl_ls *root = &s->root;
  
  for (struct sgl_ls *i = root->next; i != root; i = i->next) {
    enum sgl_cmp res = s->cmp(i, key, data);
    
    if (res != SGL_LT) {
      if (ok && res == SGL_EQ) { *ok = true; }
      return i;
    }
  }

  return root;
}
