#include <assert.h>
#include <stdio.h>

#include "snigl/list.h"
#include "snigl/pool.h"
#include "snigl/ref_type.h"
#include "snigl/sgl.h"
#include "snigl/val.h"
#include "snigl/util.h"

struct sgl_list *sgl_list_new(struct sgl *sgl) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->List);
  return sgl_list_init(sgl_malloc(&rt->pool));
}

void sgl_list_deref(struct sgl_list *l, struct sgl *sgl) {
  assert(l->nrefs > 0);
  
  if (!--l->nrefs) {
    struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->List);
    sgl_list_deinit(l, sgl);
    sgl_free(&rt->pool, l);
  }
}

struct sgl_list *sgl_list_init(struct sgl_list *l) {
  sgl_ls_init(&l->root);
  l->nrefs = 1;
  return l;  
}

struct sgl_list *sgl_list_deinit(struct sgl_list *l, struct sgl *sgl) {
  sgl_ls_do(&l->root, struct sgl_val, ls, v) { sgl_val_free(v, sgl); }
  return l;
}

sgl_int_t sgl_list_len(struct sgl_list *l) {
  sgl_int_t len = 0;
  for (struct sgl_ls *i = l->root.next; i != &l->root; i = i->next, len++);
  return len;
}

static struct sgl_val *iter_next_val(struct sgl_iter *i,
                                     struct sgl *sgl,
                                     struct sgl_pos *pos,
                                     struct sgl_ls *root) {
  struct sgl_list_iter *li = sgl_baseof(struct sgl_list_iter, iter, i);
  if ((li->pos = li->pos->next)  == &li->list->root) { return NULL; }
  return sgl_val_dup(sgl_baseof(struct sgl_val, ls, li->pos), sgl, root);
}

static bool iter_skip_vals(struct sgl_iter *i,
                           struct sgl *sgl,
                           struct sgl_pos *pos,
                           sgl_int_t nvals) {
  struct sgl_list_iter *li = sgl_baseof(struct sgl_list_iter, iter, i);
  
  for (li->pos = li->pos->next;
       li->pos != &li->list->root && --nvals;
       li->pos = li->pos->next);

  if (nvals) {
    sgl_error(sgl, pos, sgl_strdup("Iter eof"));
    return false;
  }

  return true;
}

static void iter_free(struct sgl_iter *i, struct sgl *sgl) {
  struct sgl_list_iter *li = sgl_baseof(struct sgl_list_iter, iter, i);
  sgl_list_deref(li->list, sgl);
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->ListIter);
  sgl_free(&rt->pool, li);
}

struct sgl_list_iter *sgl_list_iter_new(struct sgl *sgl, struct sgl_list *l) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, sgl->ListIter);
  struct sgl_list_iter *i = sgl_malloc(&rt->pool);
  sgl_iter_init(&i->iter);
  i->iter.next_val = iter_next_val;
  i->iter.skip_vals = iter_skip_vals;
  i->iter.free = iter_free;
  i->list = l;
  l->nrefs++;
  i->pos = &l->root;
  return i;
}

