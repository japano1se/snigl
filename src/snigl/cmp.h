#ifndef SNIGL_CMP_H
#define SNIGL_CMP_H

enum sgl_cmp {SGL_LT, SGL_EQ, SGL_GT};

enum sgl_cmp sgl_char_cmp(char lhs, char rhs);
enum sgl_cmp sgl_ptr_cmp(const void *lhs, const void *rhs);

#endif
