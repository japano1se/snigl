#ifndef SNIGL_POOL_H
#define SNIGL_POOL_H

#include "snigl/int.h"
#include "snigl/ls.h"

struct sgl_slab {
  struct sgl_ls ls;
  sgl_int_t size;
  unsigned char slots[];
};

struct sgl_pool {
  sgl_int_t slab_size, slot_size, ls_offs;
  struct sgl_ls slabs, free_slots;
  sgl_int_t offs;
};

struct sgl_pool *sgl_pool_init(struct sgl_pool *pool,
                               sgl_int_t slot_size,
                               sgl_int_t ls_offs);

struct sgl_pool *sgl_pool_deinit(struct sgl_pool *pool);

void *sgl_malloc(struct sgl_pool *pool);
void sgl_free(struct sgl_pool *pool, void *ptr);

#endif
