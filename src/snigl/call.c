#include "snigl/call.h"
#include "snigl/pool.h"
#include "snigl/sgl.h"

struct sgl_call *sgl_call_init(struct sgl_call *c,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_op *start_pc,
                               struct sgl_op *return_pc) {
  c->pos = *pos;
  if (start_pc) { sgl_state_init(&c->state, sgl); }
  c->start_pc = start_pc;
  c->return_pc = return_pc;
  return c;
}

struct sgl_call *sgl_call_new(struct sgl *sgl,
                              struct sgl_pos *pos,
                              struct sgl_op *start_pc,
                              struct sgl_op *return_pc) {
  struct sgl_call *c = sgl_call_init(sgl_malloc(&sgl->call_pool),
                                     sgl, pos, start_pc, return_pc);
  
  sgl_ls_push(&sgl->task->calls, &c->ls);
  return c;
}

void sgl_call_free(struct sgl_call *call, struct sgl *sgl) {
  sgl_ls_delete(&call->ls);
  sgl_free(&sgl->call_pool, call);
}
