#include "snigl/pair.h"
#include "snigl/val.h"

struct sgl_pair *sgl_pair_init(struct sgl_pair *p,
                               struct sgl_val *first, struct sgl_val *last) {
  p->first = first;
  p->last = last;
  return p;
}

struct sgl_pair *sgl_pair_deinit(struct sgl_pair *p, struct sgl *sgl) {
  sgl_val_free(p->first, sgl);
  sgl_val_free(p->last, sgl);
  return p;
}
