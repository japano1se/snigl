#include "snigl/timer.h"

struct sgl_timer *sgl_timer_init(struct sgl_timer *timer) {
  timer->start = clock();
  return timer;
}

sgl_int_t sgl_timer_ms(struct sgl_timer *timer) {
  return (clock() - timer->start) / (CLOCKS_PER_SEC / 1000);
}
