#include <assert.h>
#include <string.h>

#include "snigl/pool.h"
#include "snigl/sgl.h"
#include "snigl/stack.h"
#include "snigl/sym.h"
#include "snigl/trace.h"

struct sgl_trace *sgl_trace_new(struct sgl *sgl) {
  struct sgl_trace *t = sgl_malloc(&sgl->trace_pool);
  sgl_ls_init(&t->stack);
  t->len = 0;
  return t;
}

void sgl_trace_free(struct sgl_trace *t, struct sgl *sgl) {
  sgl_trace_clear(t, sgl);
  sgl_free(&sgl->trace_pool, t);
}

void sgl_trace_undef(struct sgl_trace *t, struct sgl *sgl, sgl_int_t n) {
  struct sgl_ls *s = &t->stack;

  for (struct sgl_ls *i = s->prev; n && i != s; i = i->prev, n--) {
    struct sgl_val *v = sgl_baseof(struct sgl_val, ls, i);
    struct sgl_type *vt = v->type;

    if (v->type != sgl->Undef) {
      sgl_val_reuse(v, sgl, sgl->Undef, true)->as_type = vt;
    }
  }
}

bool sgl_trace_is_undef(struct sgl_trace *t, struct sgl *sgl, sgl_int_t n) {
  struct sgl_ls *s = &t->stack;

  for (struct sgl_ls *i = s->prev; n && i != s; i = i->prev, n--) {
    struct sgl_val *v = sgl_baseof(struct sgl_val, ls, i);
    if (v->type == sgl->Undef) { return true; }
  }

  return false;
}

static void trunc_trace(struct sgl_trace *t,
                        struct sgl *sgl,
                        struct sgl_ls *ls, struct sgl_ls *lp) {
  for (struct sgl_ls *li = lp, *ln = li->next; li != ls; li = ln, ln = li->next) {
    struct sgl_val *v = sgl_baseof(struct sgl_val, ls, li);
    sgl_ls_delete(&v->ls);
    sgl_val_free(v, sgl);
    t->len--;
  }
}

void sgl_trace_sync(struct sgl_trace *lhs, struct sgl *sgl, struct sgl_trace *rhs) {
  struct sgl_ls *ls = &lhs->stack, *lp = ls->prev, *rs = &rhs->stack, *rp = rs->prev;

  for (; lp != ls && rp != rs; lp = lp->prev, rp = rp->prev) {
    struct sgl_val
      *lv = sgl_baseof(struct sgl_val, ls, lp),
      *rv = sgl_baseof(struct sgl_val, ls, rp);

    if (!sgl_val_eq(lv, sgl, rv)) { break; }
  }

  trunc_trace(lhs, sgl, ls, lp);
  trunc_trace(rhs, sgl, rs, rp);
}

void sgl_trace_copy(struct sgl_trace *src, struct sgl *sgl, struct sgl_trace *dst) {
  sgl_ls_do(&src->stack, struct sgl_val, ls, v) {
    sgl_trace_push(dst, sgl, v->op, v);
  }
  
  dst->len += src->len;
}

struct sgl_val *sgl_trace_push(struct sgl_trace *t,
                               struct sgl *sgl,
                               struct sgl_op *op,
                               struct sgl_val *val) {
  struct sgl_val *v = sgl_val_dup(val, sgl, &t->stack);
  v->op = op;
  t->len++;
  return v;
}

struct sgl_val *sgl_trace_push_new(struct sgl_trace *t,
                                   struct sgl *sgl,
                                   struct sgl_op *op,
                                   struct sgl_type *type) {
  struct sgl_val *v = sgl_val_new(sgl, type, &t->stack);
  v->op = op;
  t->len++;
  return v;
}

struct sgl_val *sgl_trace_pop(struct sgl_trace *t, struct sgl *sgl) {
  struct sgl_ls *l = sgl_ls_pop(&t->stack);
  if (!l) { return NULL; }
  struct sgl_val *v = sgl_baseof(struct sgl_val, ls, l);
  t->len--;
  
  if (v->type == sgl->Undef) {
    sgl_val_free(v, sgl);
    return NULL;
  }

  return v;
}

void sgl_trace_push_undef(struct sgl_trace *t,
                          struct sgl *sgl,
                          struct sgl_type *type) {
  sgl_val_new(sgl, sgl->Undef, &t->stack)->as_type = type;
  t->len++;
}

void sgl_trace_drop(struct sgl_trace *t, struct sgl *sgl, sgl_int_t nvals) {
  t->len -= sgl_stack_drop(&t->stack, sgl, nvals, true);
}

void sgl_trace_dup(struct sgl_trace *t, struct sgl *sgl, struct sgl_op *op) {
  struct sgl_val *v = sgl_stack_dup(&t->stack, sgl, true);

  if (v) {
    t->len++;
    if (v->type != sgl->Undef) { sgl_trace_undef(t, sgl, 3); }
  }
}

void sgl_trace_swap(struct sgl_trace *t, struct sgl *sgl) {
  if (t->len) {
    if (t->len == 1) {
      sgl_trace_clear(t, sgl);
    } else {
      sgl_stack_swap(&t->stack, sgl, true);
      sgl_trace_undef(t, sgl, 2);
    }
  }
}

void sgl_trace_rotl(struct sgl_trace *t, struct sgl *sgl) {
  if (t->len) {
    if (t->len < 3) {
      sgl_trace_clear(t, sgl);
    } else {
      sgl_stack_rotl(&t->stack, sgl, true);
      sgl_trace_undef(t, sgl, 3);
    }
  }
}

void sgl_trace_rotr(struct sgl_trace *t, struct sgl *sgl) {
  if (t->len) {
    if (t->len < 3) {
      sgl_trace_clear(t, sgl);
    } else {
      sgl_stack_rotr(&t->stack, sgl, true);
      sgl_trace_undef(t, sgl, 3);
    }
  }
}

void sgl_trace_move(struct sgl_trace *src, struct sgl_trace *dst) {
  dst->len = src->len;
  sgl_trace_move_ls(src, &dst->stack);
}

void sgl_trace_move_ls(struct sgl_trace *src, struct sgl_ls *dst) {
  struct sgl_ls *ss = &src->stack;

  if (ss->prev != ss) {
    dst->prev = ss->prev;
    dst->prev->next = dst;
  }

  if (ss->next != ss) {
    dst->next = ss->next;
    dst->next->prev = dst;
  }
  
  sgl_ls_init(ss);
  src->len = 0;
}

void sgl_trace_clear(struct sgl_trace *t, struct sgl *sgl) {
  sgl_ls_do(&t->stack, struct sgl_val, ls, v) { sgl_val_free(v, sgl); }
  sgl_ls_init(&t->stack);
  t->len = 0;
}
