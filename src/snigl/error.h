#ifndef SNIGL_ERROR_H
#define SNIGL_ERROR_H

#include "snigl/int.h"
#include "snigl/ls.h"
#include "snigl/pos.h"

struct sgl;
struct sgl_buf;
struct sgl_val;

struct sgl_error {
  struct sgl_ls ls;
  struct sgl_pos pos;
  char *msg;
  struct sgl_val *val;
  sgl_int_t nrefs;
};

struct sgl_error *sgl_error_new(struct sgl *sgl,
                                struct sgl_pos *pos,
                                char *msg,
                                struct sgl_val *val,
                                struct sgl_ls *root);

struct sgl_error *sgl_error_init(struct sgl_error *e,
                                 struct sgl_pos *pos,
                                 char *msg,
                                 struct sgl_val *val,
                                 struct sgl_ls *root);

struct sgl_error *sgl_error_deinit(struct sgl_error *e, struct sgl *sgl);
void sgl_error_free(struct sgl_error *e, struct sgl *sgl);
void sgl_error_deref(struct sgl_error *e, struct sgl *sgl);
void sgl_error_dump(struct sgl_error *e, struct sgl_buf *out);

#endif
