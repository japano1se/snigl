#ifndef SNIGL_TYPES_BUF_H
#define SNIGL_TYPES_BUF_H

struct sgl_lib;
struct sgl_str;
struct sgl_type;

struct sgl_type *sgl_buf_type_new(struct sgl *sgl,
                                  struct sgl_pos *pos,
                                  struct sgl_lib *lib,
                                  struct sgl_sym *id,
                                  struct sgl_type *parents[]);

#endif
