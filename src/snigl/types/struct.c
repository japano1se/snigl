#include <assert.h>
#include <stdio.h>

#include "snigl/buf.h"
#include "snigl/call.h"
#include "snigl/struct.h"
#include "snigl/iter.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/stack.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/struct.h"
#include "snigl/val.h"

static struct sgl_op *call_val(struct sgl_val *val,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_op *return_pc,
                               bool now) {
  if (sgl->c_fimp == sgl->call_fimp) {
    sgl_val_dup(val, sgl, sgl_stack(sgl));
    return return_pc->next;
  }

  return sgl_func_call(sgl->call_fimp->func, sgl, true, NULL);
}

static bool clone_val(struct sgl_val *src,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *dst) {
  if (sgl->c_fimp != sgl->clone_fimp &&
      sgl_func_call(sgl->clone_fimp->func, sgl, true, NULL)) {
    struct sgl_val *dv = sgl_pop(sgl);

    if (!dv) {
      sgl_error(sgl, pos, sgl_strdup("Missing cloned value"));
      return false;
    }
    
    sgl_val_deinit(dst, sgl);
    *dst = *dv;
  }

  struct sgl_struct_type *st = sgl_struct_type(src->type);
  struct sgl_field **f = sgl_vec_beg(&st->fields);
  struct sgl_struct *s = sgl_struct_new(st);
  struct sgl_val **sfs = src->as_struct->fields, **dfs = s->fields;

  for (struct sgl_val **sf = sfs, **df = dfs;
       sf < sfs + st->fields.len;
       f++, sf++, df++) {
    if (*sf) {
      *df = sgl_val_clone(*sf, sgl, pos, NULL);

      if (!*df) {
        sgl_error(sgl, pos, sgl_sprintf("Failed cloning field: %s", (*f)->id->id));
        sgl_struct_deref(s, sgl, st);
        return false;
      }
    }
  }
  
  dst->as_struct = s;
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *val, struct sgl_val *rhs) {  
  struct sgl_struct_type *st = sgl_struct_type(val->type);
  struct sgl_field **f = sgl_vec_beg(&st->fields);
  struct sgl_val **vfs = val->as_struct->fields, **rfs = rhs->as_struct->fields;

  for (struct sgl_val **vf = vfs, **rf = rfs;
       vf < vfs + st->fields.len;
       f++, vf++, rf++) {
    enum sgl_cmp ok = sgl_val_cmp(*vf, *rf);
    if (ok != SGL_EQ) { return ok; }
  }

  return SGL_EQ;
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  sgl_struct_deref(val->as_struct, sgl, sgl_struct_type(val->type));
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_printf(out, "(%s", val->type->def.id->id);
  struct sgl_struct_type *st = sgl_struct_type(val->type);

  for (struct sgl_val **fs = val->as_struct->fields, **v = fs;
       v < fs + st->fields.len;
       v++) {
    if (*v) {
      sgl_buf_putc(out, ' ');
      sgl_val_dump(*v, out);
    } else {
      sgl_buf_putcs(out, " _");
    }
  }
  
  sgl_buf_putc(out, ')');
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_struct *s = out->as_struct = val->as_struct;
  s->nrefs++;
}

static bool eq_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *rhs) {
  if (sgl->c_fimp != sgl->eq_fimp &&
      sgl_func_call(sgl->eq_fimp->func, sgl, true, val, rhs)) {
    return sgl_pop(sgl)->as_bool;
  }
  
  struct sgl_struct_type *st = sgl_struct_type(val->type);
  struct sgl_field **f = sgl_vec_beg(&st->fields);
  struct sgl_val **vfs = val->as_struct->fields, **rfs = rhs->as_struct->fields;

  for (struct sgl_val **vf = vfs, **rf = rfs;
       vf < vfs + st->fields.len;
       f++, vf++, rf++) {
    struct sgl_val *vv = *vf, *rv = *rf;
    if (!vv && !rv) { continue; }
    if (!vv || !rv || !sgl_val_eq(vv, sgl, rv)) { return false; }
  }

  return true;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_struct == rhs->as_struct;
}

static struct sgl_iter *iter_val(struct sgl_val *v,
                                 struct sgl *sgl,
                                 struct sgl_type **type) {
  if (sgl->c_fimp != sgl->iter_fimp &&
      sgl_func_call(sgl->iter_fimp->func, sgl, true, v)) {
    struct sgl_val *i = sgl_pop(sgl);
    if (!i) { return NULL; }
    
    if (!sgl_derived(i->type, sgl->Iter)) {
      sgl_error(sgl, sgl_pos(sgl),
                sgl_sprintf("Expected Iter, was: %s", i->type->def.id->id));
      
      return NULL;
    }
    
    if (type) { *type = i->type; }
    struct sgl_iter *it = i->as_iter;
    it->nrefs++;
    sgl_val_free(i, sgl);
    return it;
  }

  sgl_error(sgl, sgl_pos(sgl), sgl_sprintf("Missing iter: %s", v->type->def.id->id));
  return NULL;
}

static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  sgl_buf_printf(out, "(%s %p)", val->type->def.id->id, val->as_struct);
}

static enum sgl_cmp field_set_cmp(struct sgl_ls *lhs, const void *rhs, void *data) {
  struct sgl_sym *lsym = sgl_baseof(struct sgl_field, ls, lhs)->id;
  return sgl_sym_cmp(lsym, rhs);
}

void free_imp(struct sgl_type *t, struct sgl *sgl) {
  struct sgl_struct_type *st = sgl_struct_type(t);
  sgl_vec_do(&st->fields, struct sgl_field *, f) { sgl_field_free(*f, sgl); } 
  sgl_vec_deinit(&st->fields);
  sgl_free(&sgl->struct_type_pool, st);
  sgl_ref_type_deinit(&st->ref_type);
}

struct sgl_struct_type *sgl_struct_type(struct sgl_type *t) {
  struct sgl_ref_type *rt = sgl_baseof(struct sgl_ref_type, type, t);
  return sgl_baseof(struct sgl_struct_type, ref_type, rt);
}

struct sgl_type *sgl_struct_type_new(struct sgl *sgl,
                                     struct sgl_pos *pos,
                                     struct sgl_lib *lib,
                                     struct sgl_sym *id,
                                     struct sgl_type *parents[]) {
  struct sgl_struct_type *st = sgl_malloc(&sgl->struct_type_pool);
  
  sgl_lset_init(&st->field_set, field_set_cmp);
  sgl_vec_init(&st->fields, sizeof(struct sgl_field *));
  
  sgl_ref_type_init(&st->ref_type, sgl, pos, lib, id, parents,
                    sizeof(struct sgl_struct),
                    offsetof(struct sgl_struct, ls));

  for (struct sgl_type **p = parents; *p; p++) {
    if (*p != sgl->Struct && sgl_derived(*p, sgl->Struct)) {
      struct sgl_ref_type *prt = sgl_baseof(struct sgl_ref_type, type, *p);
      struct sgl_struct_type *pst = sgl_baseof(struct sgl_struct_type, ref_type, prt);
      sgl_vec_do(&pst->fields, struct sgl_field *, f) {
        if (!sgl_struct_type_add(st, sgl, pos, (*f)->id, (*f)->type)) {
          sgl_free(&sgl->struct_type_pool, st);
          return NULL;
        }
      }
    }
  }
  
  struct sgl_type *t = &st->ref_type.type;
  t->call_val = call_val;
  t->clone_val = clone_val;
  t->cmp_val = cmp_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->eq_val = eq_val;
  t->is_val = is_val;
  t->iter_val = iter_val;
  t->print_val = print_val;
  t->free = free_imp;

  sgl_derive(sgl_type_meta(t, sgl), sgl_type_meta(sgl->Struct, sgl));
  return t;
}

struct sgl_field *sgl_struct_type_find(struct sgl_struct_type *t,
                                       struct sgl_sym *id) {
  bool ok = false;
  struct sgl_ls *found = sgl_lset_find(&t->field_set, id, NULL, &ok);
  return ok ? sgl_baseof(struct sgl_field, ls, found) : NULL;
}

struct sgl_field *sgl_struct_type_add(struct sgl_struct_type *t,
                                      struct sgl *sgl,
                                      struct sgl_pos *pos,
                                      struct sgl_sym *id,
                                      struct sgl_type *type) {
  bool ok = false;
  struct sgl_ls *ls = sgl_lset_find(&t->field_set, id, NULL, &ok);
  
  if (ok) {
    sgl_error(sgl, pos, sgl_sprintf("Dup field: %s", id));
    return NULL;
  }

  assert(sgl_ls_null(&t->ref_type.pool.slabs));
  t->ref_type.pool.slot_size += sizeof(struct sgl_val *);
  return sgl_field_new(sgl, t, id, type, ls);
}

struct sgl_field *sgl_field_new(struct sgl *sgl,
                                struct sgl_struct_type *struct_type,
                                struct sgl_sym *id,
                                struct sgl_type *type,
                                struct sgl_ls *next) {
  struct sgl_field *f = sgl_malloc(&sgl->field_pool);
  f->i = struct_type->fields.len;
  f->id = id;
  f->type = type;

  sgl_ls_insert(next, &f->ls);
  *(struct sgl_field **)sgl_vec_push(&struct_type->fields) = f;
  return f;
}

void sgl_field_free(struct sgl_field *f, struct sgl *sgl) {
  sgl_free(&sgl->field_pool, f);
}
                    
