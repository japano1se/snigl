#ifndef SNIGL_TYPES_CHAR_H
#define SNIGL_TYPES_CHAR_H

struct sgl_lib;
struct sgl_sym;
struct sgl_type;

struct sgl_type *sgl_char_type_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   struct sgl_type *parents[]);

#endif
