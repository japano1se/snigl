#include <stdio.h>

#include "snigl/buf.h"
#include "snigl/call.h"
#include "snigl/form.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/sym.h"
#include "snigl/type.h"
#include "snigl/types/lambda.h"
#include "snigl/val.h"

static struct sgl_op *call_val(struct sgl_val *val,
                               struct sgl *sgl,
                               struct sgl_pos *pos,
                               struct sgl_op *return_pc,
                               bool now) {
  struct sgl_op_lambda *l = val->as_lambda;
  if (!l->start_pc) { return return_pc->next; }
  if (l->end_pc == l->start_pc) { return return_pc->next; }
  
  if (now) {    
    sgl_call_new(sgl, pos, NULL, sgl->end_pc->prev);
    
    return sgl_run_task(sgl, sgl->task, l->start_pc->next, l->end_pc->next)
      ? return_pc->next
      : sgl->end_pc;
  }

  sgl_call_new(sgl, pos, NULL, return_pc);
  return l->start_pc->next;
}

static enum sgl_cmp cmp_val(struct sgl_val *val, struct sgl_val *rhs) {
  return sgl_ptr_cmp(val->as_lambda, rhs->as_lambda);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  struct sgl_op_lambda *l = val->as_lambda;
  struct sgl_pos *pos = &l->start_pc->form->pos;
  sgl_buf_printf(out,
                 "(%s %" SGL_INT " %" SGL_INT ")",
                 sgl_type_id(val->type)->id, pos->row, pos->col);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  out->as_lambda = val->as_lambda;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_lambda == rhs->as_lambda;
}

struct sgl_type *sgl_lambda_type_new(struct sgl *sgl,
                                     struct sgl_pos *pos,
                                     struct sgl_lib *lib,
                                     struct sgl_sym *id,
                                     struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->call_val = call_val;
  t->cmp_val = cmp_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  return t;
}
