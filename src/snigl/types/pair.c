#include "snigl/buf.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/type.h"
#include "snigl/types/pair.h"
#include "snigl/val.h"

static bool bool_val(struct sgl_val *val) {
  struct sgl_pair *p = &val->as_pair;
  return sgl_val_bool(p->first) || sgl_val_bool(p->last);
}

static bool clone_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_val *out) {
  struct sgl_pair *p = &val->as_pair;

  struct sgl_val
    *fv = sgl_val_clone(p->first, sgl, pos, NULL),
    *lv = sgl_val_clone(p->last, sgl, pos, NULL);

  if (!fv || !lv) { return false;}
  sgl_pair_init(&out->as_pair, fv, lv);
  return true;
}

static enum sgl_cmp cmp_val(struct sgl_val *val, struct sgl_val *rhs) {
  struct sgl_pair *lp = &val->as_pair, *rp = &rhs->as_pair;
  enum sgl_cmp res = sgl_val_cmp(lp->first, rp->first);
  if (res != SGL_EQ) { return res; }
  return sgl_val_cmp(lp->last, rp->last);
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  sgl_pair_deinit(&val->as_pair, sgl);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  struct sgl_pair *p = &val->as_pair;
  sgl_val_dump(p->first, out);
  sgl_buf_putcs(out, "::");
  sgl_val_dump(p->last, out);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sgl_pair *p = &val->as_pair;

  sgl_pair_init(&out->as_pair,
                sgl_val_dup(p->first, sgl, NULL),
                sgl_val_dup(p->last, sgl, NULL));
}

static bool eq_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *rhs) {
  struct sgl_pair *p = &val->as_pair, *rp = &rhs->as_pair;
  return sgl_val_eq(p->first, sgl, rp->first) && sgl_val_eq(p->last, sgl, rp->last);
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  struct sgl_pair *vp = &val->as_pair, *rp = &rhs->as_pair;
  return sgl_val_is(vp->first, rp->first) && sgl_val_is(vp->last, rp->last);
}

static void print_val(struct sgl_val *val,
                      struct sgl *sgl,
                      struct sgl_pos *pos,
                      struct sgl_buf *out) {
  struct sgl_pair *p = &val->as_pair;
  sgl_val_print(p->first, sgl, pos, out);
  sgl_val_print(p->last, sgl, pos, out);
}

struct sgl_type *sgl_pair_type_new(struct sgl *sgl,
                                   struct sgl_pos *pos,
                                   struct sgl_lib *lib,
                                   struct sgl_sym *id,
                                   struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->bool_val = bool_val;
  t->is_val = is_val;
  t->clone_val = clone_val;
  t->cmp_val = cmp_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->eq_val = eq_val;
  t->print_val = print_val;
  return t;
}
