#include <assert.h>
#include <string.h>

#include "snigl/struct.h"
#include "snigl/types/struct.h"
#include "snigl/val.h"

struct sgl_struct *sgl_struct_new(struct sgl_struct_type *type) {
  return sgl_struct_init(sgl_malloc(&type->ref_type.pool), type);
}

struct sgl_struct *sgl_struct_init(struct sgl_struct *s,
                                   struct sgl_struct_type *type) {
  memset(s->fields, 0, sizeof(struct sgl_val *) * type->fields.len);
  s->nrefs = 1;
  return s;
}

struct sgl_struct *sgl_struct_deinit(struct sgl_struct *s,
                                     struct sgl *sgl,
                                     struct sgl_struct_type *type) {
  for (struct sgl_val **v = s->fields; v < s->fields + type->fields.len; v++) {
    if (*v) { sgl_val_free(*v, sgl); }
  }
  
  return s;
}

void sgl_struct_deref(struct sgl_struct *s,
                      struct sgl *sgl,
                      struct sgl_struct_type *type) {
  assert(s->nrefs > 0);
  
  if (!--s->nrefs) {
    sgl_struct_deinit(s, sgl, type);
    sgl_free(&type->ref_type.pool, s);
  }
}
