#include <assert.h>
#include <stdatomic.h>
#include <stdio.h>
#include <time.h>

#include "snigl/io.h"
#include "snigl/io_thread.h"
#include "snigl/pool.h"
#include "snigl/sgl.h"

struct sgl_io_thread *sgl_io_thread_new(struct sgl *sgl) {
  struct sgl_io_thread *t = sgl_malloc(&sgl->io_thread_pool);
  t->sgl = sgl;
  t->stopped = false;
  sgl_ls_push(&sgl->io_threads, &t->ls);
  sgl_ls_init(&t->queue);
  pthread_mutex_init(&t->queue_lock, NULL);
  pthread_cond_init(&t->queue_cond, NULL);
  return t;
}

void sgl_io_thread_free(struct sgl_io_thread *t, struct sgl *sgl) {
  pthread_mutex_destroy(&t->queue_lock);
  pthread_cond_destroy(&t->queue_cond);
  sgl_ls_delete(&t->ls);
  sgl_free(&sgl->io_thread_pool, t);
}

static void *thread_main(void *data) {
  struct sgl_io_thread *t = data;
  struct sgl *sgl = t->sgl;
  pthread_mutex_lock(&t->queue_lock);
  
  while (!atomic_load(&t->stopped)) {
    if (t->queue.next == &t->queue) {
      atomic_fetch_add(&sgl->nio_idle_threads, 1);
      pthread_cond_wait(&t->queue_cond, &t->queue_lock);
      atomic_fetch_sub(&sgl->nio_idle_threads, 1);
      continue;
    }

    struct sgl_io *io = sgl_baseof(struct sgl_io, ls, sgl_ls_pop(&t->queue));
    pthread_mutex_unlock(&t->queue_lock);
    io->error = io->run(io, sgl, t);
    pthread_mutex_lock(&sgl->io_lock);
    sgl_ls_push(&sgl->io_queue, &io->ls);
    pthread_mutex_unlock(&sgl->io_lock);
    atomic_fetch_add(&sgl->nio_done, 1);
    pthread_cond_signal(&sgl->io_done);
    pthread_mutex_lock(&t->queue_lock);
  }
  
  pthread_mutex_unlock(&t->queue_lock);
  atomic_store(&t->stopped, false);
  return NULL;
}

bool sgl_io_thread_start(struct sgl_io_thread *t) {
  pthread_create(&t->imp, NULL, thread_main, t);
  return true;
}

bool sgl_io_thread_stop(struct sgl_io_thread *t) {
  atomic_store(&t->stopped, true);
  while (atomic_load(&t->stopped)) { pthread_cond_signal(&t->queue_cond); }
  pthread_join(t->imp, NULL);
  return true;
}

bool sgl_io_thread_push(struct sgl_io_thread *t, struct sgl_io *io) {
  pthread_mutex_lock(&t->queue_lock);
  sgl_ls_push(&t->queue, &io->ls);
  pthread_mutex_unlock(&t->queue_lock);
  pthread_cond_signal(&t->queue_cond);
  return true;
}
