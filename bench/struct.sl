struct: Foo _ ('bar 'baz 'qux Int)

10 bench: (100000 times:,
  Foo new
  dup put: ('.Foo/bar :: 40)
  dup dup .Foo/bar ++ swap put: '.Foo/baz
  dup .Foo/baz ++ swap put: '.Foo/qux
) say