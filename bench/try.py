from bench import bench

print(bench(10, '''
def test():
  try:
    return None
  except Exception as e:
    return e
''', '''
for _ in range(100000):
  test()
'''))

