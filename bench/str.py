from bench import bench

print(bench(10, '', '''
s = []

for _ in range(100000):
  s.append(", ".join(["foo", "bar", str(42)]))

";".join(s)
'''))
