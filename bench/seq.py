from bench import bench

print(bench(10, '', '''
for _ in range(1000):
  [x+1 for x in range(100) if x > 50]
'''))

print(bench(10, '', '''
for _ in range(1000):
  list(map(lambda x: x+1, filter(lambda x: x > 50, range(100))))
'''))
