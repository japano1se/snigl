from bench import bench

print(bench(10, '''
def foo(x): pass
''', '''
for _ in range(100000):
  foo("foo {0} baz {1}".format("bar", 42))
'''))
