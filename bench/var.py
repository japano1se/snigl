from bench import bench

print(bench(10, '', '''
def test():
  foo = 42
  bar = foo
  baz = bar

for _ in range(100000):
  test()
'''))
